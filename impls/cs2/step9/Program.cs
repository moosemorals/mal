﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

using static uk.osric.mal.Core;

namespace uk.osric.mal {
    public class Program {
        private readonly Env repl_env = new(null);
        private readonly Reader reader = new Reader();

        private IMalType Quasiquote(IMalType ast, bool wasVec = false) {
            if (ast is MalList l) {
                if (l.IsEmpty) {
                    return l;
                }
                if (!wasVec && l.First is MalSymbol { Value: "unquote" }) {
                    return l.Rest.First;
                }

                MalList result = MalList.Empty();
                foreach (IMalType elt in l.Reverse()) {
                    if (elt is MalList eltList && eltList.First is MalSymbol { Value: "splice-unquote" }) {
                        result = new MalList(new IMalType[] {
                            new MalSymbol("concat"),
                            eltList.Rest.First,
                            result
                        });
                    } else {
                        result = new MalList(new IMalType[] {
                            new MalSymbol("cons"),
                            Quasiquote(elt),
                            result
                        });
                    }
                }
                return result;
            } else if (ast is MalVector v) {
                return new MalList(new IMalType[] { new MalSymbol("vec"), Quasiquote(new MalList(v), true) });
            } else if (ast is MalSymbol || ast is MalHash) {
                return new MalList(new IMalType[] { new MalSymbol("quote"), ast });
            } else {
                return ast;
            }
        }

        private bool IsMacroCall(Env env, IMalType ast) {
            return ast is MalList seq
                && seq.First is MalSymbol s
                && env.Has(s)
                && env.Get(s) is MalMalFunc f
                && f.IsMacro;
        }

        private (IMalType, bool) MacroExpand(Env env, IMalType ast) {
            bool changed = false;
            while (IsMacroCall(env, ast)) {
                MalList seq = (MalList)ast;
                MalSymbol s = (MalSymbol)seq.First();
                MalMalFunc f = (MalMalFunc)(env.Get(s));
                ast = f.Func(seq.Rest);
                changed = true;
            }
            return (ast, changed);
        }

        private IMalType Swap(MalList ast) {
            MalAtom a = (MalAtom)ast.First;
            IMalType f = ast.Rest.First;
            MalList args = ast.Rest.Rest.Cons(a.Value);
            IMalType result;
            if (f is MalNativeFunc native) {
                result = native.Apply(args);
            } else if (f is MalMalFunc func) {
                result = func.Func(args);
            } else {
                throw new MalRuntimeException($"Can't invoke {Printer.PrStr(f, true)}: Not a function");
            }
            a.Set(result);
            return result;
        }

        private IMalType EvalAst(Env env, IMalType ast) {
            if (ast is MalSymbol s) {
                return env.Get(s);
            } else if (ast is MalList l) {
                return new MalList(l.Select(m => Eval(env, m)));
            } else if (ast is MalVector v) {
                return new MalVector(v.Select(m => Eval(env, m)));
            } else if (ast is MalHash h) {
                MalHash result = new();
                foreach (var kv in h) {
                    IMalType key = kv.Key;
                    IMalType value = Eval(env, kv.Value);
                    result.Add(key, value);
                }
                return result;
            } else {
                return ast;
            }
        }

        private IMalType Eval(Env env, IMalType ast) {
            while (true) {
                if (ast is MalList l) {
                    if (l.IsEmpty) {
                        return l;
                    }

                    // Macro expansion
                    (ast, bool changed) = MacroExpand(env, ast);
                    if (changed) {
                        continue;
                    }

                    // Special forms
                    if (l.First is MalSymbol symb) {
                        MalList rest = l.Rest;
                        switch (symb.Value) {
                            case "def!":
                                return env.Set((MalSymbol)rest.First, Eval(env, rest.Rest.First));
                            case "defmacro!": {
                                    MalMalFunc func = (MalMalFunc)Eval(env, rest.Rest.First);
                                    func.IsMacro = true;
                                    return env.Set((MalSymbol)rest.First, func);
                                }
                            case "do": {
                                    // This must evaluate right away
                                    rest.SkipLast(1).Select(m => Eval(env, m)).ToList();
                                    ast = rest.Last();
                                    continue;
                                }
                            case "if": {
                                    IMalType testResult = Eval(env, rest.First);
                                    if (testResult != IMalType.Nil && testResult != IMalType.False) {
                                        ast = rest.Rest.First;
                                    } else {
                                        ast = rest.Rest.Rest.First;
                                    }
                                    continue;
                                }
                            case "let*": {
                                    Env inner = new Env(env);
                                    if (rest.First is MalList bindingList) {
                                        // via https://stackoverflow.com/a/6888263
                                        using (var iterator = bindingList.GetEnumerator()) {
                                            while (iterator.MoveNext()) {
                                                IMalType key = iterator.Current;
                                                IMalType value = iterator.MoveNext() ? iterator.Current : throw new MalRuntimeException("Bindings must come in pairs");
                                                inner.Set((MalSymbol)key, Eval(inner, value));
                                            }
                                        }
                                    } else if (rest.First is MalVector bindingVector) {
                                        for (int i = 0; i < bindingVector.Count; i += 2) {
                                            MalSymbol key = (MalSymbol)bindingVector[i];
                                            IMalType value = bindingVector[i + 1];
                                            inner.Set(key, Eval(inner, value));
                                        }
                                    } else {
                                        throw new MalRuntimeException("Unknown type for 'let*' bindings");
                                    }
                                    env = inner;
                                    ast = rest.Rest.First;
                                    continue;
                                }
                            case "fn*":
                                return new MalMalFunc(
                                    env,
                                    rest.Rest.First,
                                    (IMalSeq)rest.First,
                                    l => Eval(new Env(env, (IMalSeq)rest.First, l), rest.Rest.First)
                                );
                            case "try*":
                                try {
                                    return Eval(env, rest.First);
                                } catch (Exception ex) {
                                    MalList catchList = (MalList)(rest.Rest.First);
                                    MalSymbol b = (MalSymbol)(catchList.Rest.First);
                                    IMalType c = catchList.Rest.Rest.First;
                                    Env catchEnv = new Env(env);
                                    catchEnv.Set(b, new MalString(ex.Message));
                                    return Eval(catchEnv, c);
                                }
                            case "quote":
                                return rest.First;
                            case "quasiquote":
                                ast = Quasiquote(rest.First);
                                continue;
                            case "quasiquoteexpand":
                                return Quasiquote(rest.First);
                            case "macroexpand": {
                                    (IMalType expanded, _) = MacroExpand(env, rest.First);
                                    return expanded;
                                }
                        }
                    }

                    // Call
                    MalList list = (MalList)EvalAst(env, ast);
                    if (list.First is MalNativeFunc native) {
                        return native.Apply(list.Rest);
                    } else if (list.First is MalMalFunc func) {
                        ast = func.Body;
                        env = new Env(func.Env, func.Param, list.Rest);
                    } else {
                        throw new MalRuntimeException($"Can't invoke '{Printer.PrStr(list.First, true)}': Not a function");
                    }
                } else {
                    return EvalAst(env, ast);
                }
            }
        }

        private IMalType Read(string input) {
            return reader.ReadStr(input);
        }

        private string Print(IMalType input) {
            return Printer.PrStr(input, true);
        }

        private string Rep(string input) {
            return Print(Eval(repl_env, Read(input)));
        }

        private void LoadStdLib(Env env) {
            foreach (MethodInfo mi in typeof(Core).GetMethods()) {
                CoreFuncAttribute? cfa = mi.GetCustomAttribute<CoreFuncAttribute>();
                if (cfa == null) {
                    continue;
                }
                env.Set(
                    new MalSymbol(cfa.Name),
                    new MalNativeFunc(mi.CreateDelegate<MalFunc>())
                );
            }

            // Load standard lib mal functions
            foreach (string def in Core.Mal) {
                Rep(def);
            }

            // Stuff thats easier to just define here
            env.Set(new MalSymbol("eval"), new MalNativeFunc(ast => Eval(env, ast.First)));
            env.Set(new MalSymbol("swap!"), new MalNativeFunc(ast => Swap(ast)));
        }

        public void Repl(string[] args) {
            LoadStdLib(repl_env);

            repl_env.Set(
                new MalSymbol("*ARGV*"),
                new MalList(args.Skip(1).Select(s => new MalString(s)))
            );

            if (args.Length > 0) {
                Rep($"(load-file \"{args[0]}\")");
                return;
            }

            Readline readline = new();

            while (true) {
                try {
                    string? input = readline.WaitForInput("user> ", basic: true);
                    if (input != null) {
                        Console.WriteLine(Rep(input));
                    }
                } catch (MalException ex) {
                    Console.Error.WriteLine($"(There was a problem {ex.Message}");
                    Console.Error.WriteLine(ex.StackTrace?.Split("\n").First() ?? "Location unknown");
                } catch (Exception ex) {
                    Console.Error.WriteLine($"There was a problem {ex.Message}");
                    Console.Error.WriteLine(Printer.FormatException(ex));
                }
            }
        }

        public static void Main(string[] args) {
            Program mal = new Program();
            mal.Repl(args);
        }
    }
}