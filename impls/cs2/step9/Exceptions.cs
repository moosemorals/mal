
using System;

namespace uk.osric.mal {

    public class MalException : Exception {
        public MalException(string messgae) : base(messgae) { }
    }

    public class MalParseException : MalException {
        public MalParseException(string message) : base(message) { }
    }

    public class MalRuntimeException : MalException {
        public MalRuntimeException(string message) : base(message) { }
    }

    public class MalUserException : MalException {
        public IMalType Value { get; init; }
        public MalUserException(IMalType value) : base(Printer.PrStr(value, true)) {
            Value = value;
        }
    }


}