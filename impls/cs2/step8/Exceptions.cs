
using System;

namespace uk.osric.mal {

    public class MalException : Exception {
        public MalException(string v) : base(v) { }
    }


    public class MalParseException : MalException {
        public MalParseException(string v) : base(v) { }
    }

    public class MalRuntimeException : MalException {
        public MalRuntimeException(string v) : base(v) { }
    }

    public class MalInternalException : MalException {
        public MalInternalException(string v) : base(v) { }
     }


}